# Contour-Detection
Implementation of the Pavlidis algorithm to detect contours from scratch. 

Also, using the Discrete Curve Evolution (DCE) algorithm to reduce the number of redundant points in the detected contour image.
